export CC = gcc
export LD = gcc

export SRC_DIR = ./src
export OBJ_DIR = ./obj
export BIN_DIR = ./bin

export CFLAGS = -W -Wall -Wextra -pedantic -std=c99 -Werror
export LFLAGS = 

VPATH = $(shell find $(SRC_DIR) -type d)

SRC = $(wildcard $(SRC_DIR)/*.c)
OBJ = $(addprefix $(OBJ_DIR)/, $(notdir $(SRC:.c=.c.o)))
EXE = $(BIN_DIR)/abs

all: dirs $(EXE)

dirs:
	@mkdir -p $(SRC_DIR) $(OBJ_DIR) $(BIN_DIR)

clean:
	@echo "CLEAN"
	@rm -rf $(OBJ_DIR)

mrproper:
	@echo "MRPROPER"
	@rm -rf $(OBJ_DIR) $(BIN_DIR)

$(EXE): $(OBJ)
	$(LD) -o $@ $^ $(LFLAGS)

$(OBJ_DIR)/%.c.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)
